###Build images (profiles) for VmWare Studio 2.6

Hello! Here are short xml profiles to build favorite CentOS images (OVF 1.0 etc) using VmWare Studio 2.6


##Common Setup

*	Get VmWare Studio 2.6 from [http://www.vmware.com/products/studio](http://www.vmware.com/products/studio) (ignore "Trial" word - it is still free product (so far))

*	Deploy VmWare Studio on  supported Environment (ESXi/vSphere 5.x, vCloud Director or VmWare Workstation).

*	Wget ISO image of CentOS 6.4 (DVD1)
 
		:::bash
		cd /opt/vmware/www/ISV/ISO/
		wget http://ftp.linux.cz/pub/linux/centos/6.4/isos/x86_64/CentOS-6.4-x86_64-bin-DVD1.iso
 

## Setup of centos64_base.xml
It is very basic CentOS 6.4 x86/64bit with just few favorite packages (vim-enhanced,git,tcpdump etc).

* log to your VmWare Studio (unix/ssh login) as root

* Copy centos64_base.xml:
 
		:::bash
		cp centos64_base.xml /opt/vmware/var/lib/build/profiles/


* log in to VmWare web ui: http://YOUR_VM_WARE_HOST, login: root, password: root password you specified on 1st boot

* click on *VMs*

* click on profile *centos64_base*

* click on *Edit Profile*

* select tab *OS*

* fill in Root password

* select tab *Build*

* fill in parameters of your build environment 

* click on Save and build

* after a while there should be OVF 0.9 and OVF 1.0 images build (web ui shows URL for them)

* now you can deploy and boot that image

That's all








